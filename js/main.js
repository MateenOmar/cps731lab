$(function(){
    console.log("MAIN SCRIPT IS RUNNING");
    $(".alert-success").hide();
    $(".alert-danger").hide();

    // Get the modal
    var loginModal = document.getElementById("loginModal");
    var signupModal = document.getElementById("signupModal");
    var removeCourseModal = document.getElementById("removeCourseModal");
    var addCourseModal = document.getElementById("addCourseModal");

    // Get the button that opens the modal
    //var loginBtn = document.getElementById("loginBtn");
    //var signupBtn = document.getElementById("signupBtn");
    //var submitLoginBtn = document.getElementById("submitLoginBtn");

    // Get the <span> element that closes the modal
    //var span = document.getElementsByClassName("close")[0];

    // When the user clicks on the button, open the modal
    $('#loginBtn').click(function() {
        loginModal.style.display = "block";
    });

    $('#signupBtn').click(function() {
        signupModal.style.display = "block";
    });

    $('#removeCourseBtn').click(function() {
        removeCourseModal.style.display = "block";
    });

    $('#addCourseBtn').click(function() {
        addCourseModal.style.display = "block";
    });

    $('#calculateBtn').click(function() {
        console.log("do math");
    });

    $('#submitLoginBtn').click(function() {
        //self.location.replace("/../home.html");
        loginModal.style.display = "none";
        self.open("/../home.html");
    });

    $('#submitSignupBtn').click(function() {
        $(".alert-success").slideDown(500).delay(2000).slideUp(500);
        $(".alert-danger").slideDown(500).delay(2000).slideUp(500);
        signupModal.style.display = "none";
    });

    $('#submitRemoveCourseBtn').click(function() {
        removeCourseModal.style.display = "none";
        alert("Course Removed");
    });

    $('#submitAddCourseBtn').click(function() {
        addCourseModal.style.display = "none";
        alert("Course Added");
    });

    // When the user clicks on (x), close the modal
    $('span').click(function() {
        loginModal.style.display = "none";
    });
    $('span').click(function() {
        signupModal.style.display = "none";
    });
    $('span2').click(function() {
        removeCourseModal.style.display = "none";
    });
    $('span2').click(function() {
        addCourseModal.style.display = "none";
    });


    // When the user clicks anywhere outside of the modal, close it
    self.onclick = function(event) {
        if (event.target == loginModal) {
            loginModal.style.display = "none";
        }
        else if (event.target == signupModal){
            signupModal.style.display = "none";
        }
        else if (event.target == removeCourseModal){
            removeCourseModal.style.display = "none";
        }
        else if (event.target == addCourseModal){
            addCourseModal.style.display = "none";
        }
    }

});